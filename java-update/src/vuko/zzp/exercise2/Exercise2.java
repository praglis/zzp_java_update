package vuko.zzp.exercise2;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

public class Exercise2 {

    public static void main(String[] args) {
        var integerCollection = new HashSet<Integer>();
        integerCollection.add(232);
        integerCollection.add(1459);
        integerCollection.add(-1483);

        Set<Integer> unmodifiableCollection = Collections.unmodifiableSet(integerCollection);

        unmodifiableCollection.stream().filter(i -> i > 0).forEach(i -> System.out.println("Positive member of collection: " + i));

        unmodifiableCollection.add(127);
        // próba dodania elementu zakończyła się wyrzuceniem wyjątku, gdyż nie można modyfikować unmodifiable collections
    }
}
