package vuko.zzp.exercise1;

import java.util.List;

interface ExampleInterface {
    private List<Integer> privateMethod() {
        return List.of(-339, -1105, -1032, 1228, 1371);
    }

    default void defaultMethod() {
        int firstPositiveElement = privateMethod().stream().filter(integer -> integer > 0).findFirst().get();
        System.out.println("First positive integer from private method is: " + firstPositiveElement);
    }
}
