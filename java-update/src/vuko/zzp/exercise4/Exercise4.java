package vuko.zzp.exercise4;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

public class Exercise4 {
    public static void main(String[] args) throws IOException {

        Path originalPath = Files.writeString(Files.createTempFile("example", ".txt"), "This is content of the file.");
        Path identicalPath = Files.writeString(Files.createTempFile("example", ".txt"), "This is content of the file.");
        Path differentPath = Files.writeString(Files.createTempFile("example", ".txt"), "This is SPARTA!");

        compareFiles(originalPath, identicalPath);
        compareFiles(originalPath, differentPath);
    }

    private static void compareFiles(Path path1, Path path2) throws IOException {
        long mismatchOutput = Files.mismatch(path1, path2);
        if (mismatchOutput == -1) {
            System.out.println("The files are identical.");
        } else {
            System.out.println("The files are different. First difference at position " + mismatchOutput);
        }
        System.out.println(""" 
                                
                The first file:
                ---------------
                """ + Files.readString(path1) + """
                                
                ---------------
                                
                The second file:
                ----------------
                """ + Files.readString(path2) +
                "\n----------------"
        );
    }
}
