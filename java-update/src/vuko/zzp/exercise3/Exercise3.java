package vuko.zzp.exercise3;

import java.util.concurrent.atomic.AtomicInteger;

public class Exercise3 {

    public static void main(String[] args) {
        // isblank() zwraca true, jeśli string jest pusty bądź składa się z samych białych znaków
        System.out.println("Is string blank: " + " ".isBlank());

        // isEmpty() zwraca true, jeśli długość stringu wynosi 0
        System.out.println("Is string empty: " + " ".isEmpty());

        /*
         lines() dzieli string wykorzystując znaki nowej linii (\n, \r a także ich kombinację) i przekształca je w
         strumień. Analogiczna metoda do stream() w kolekcjach.
         */
        AtomicInteger i = new AtomicInteger();
        "AB\nAB\nAB".lines().forEach(s -> System.out.println("Line no. " + i.getAndIncrement() + ": " + s));

        /*
        stripLeading() usuwa białe znaki na początku stringu (do wystąpienia pierwszego "niebiałego" znaku), podczas
        gdy stripTrailing() - na końcu (po ostatnim "niebiałym" znaku). Metoda strip() jest połączeniem obu tych metod.
         */
        System.out.println("After stripping both leading and trailing whitespace characters: _" + " DB ".strip() + "_");
        System.out.println("After stripping leading whitespace characters: _" + " DB ".stripLeading() + "_");
        System.out.println("After stripping trailing whitespace characters: _" + " DB ".stripTrailing() + "_");

        /*
         repeat() tworzy string będący wielokrotnością stringu, z którego wywołano tę metodę. Działa podobnie jak
         concat() wywołany odpowiednią ilość razy.
         */
        System.out.println("Five repeats of '==': " + "==".repeat(5));

    }
}
